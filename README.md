# Livechat

There are 2 folders, one is for server side and another is for clint side.

# 1) Server

This folder is containing code for backend.

**Technology Use**: NodeJS,  Express,  Socket IO

If any PORT on .env is not set then by default it will running on PORT: 3000

**Getting Started** 
        
    1. yarn install
    2. yarn start
    3. All done! move to clint folder

# 2) Clint 

This folder is holding the code base of front end.

**Technology Use** : React JS, socket.io-client , dotenv 

**Getting Started** 
        
    1. yarn install
    2. Open .env  update the REACT_APP_SERVER_URL as per the backend path 
    3. yarn start
    4. All done

